package pl.edu.pwr.controller;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.edu.pwr.model.Template;
import pl.edu.pwr.model.TemplateDTO;
import pl.edu.pwr.service.TemplateService;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);		
	
	@Autowired
	private TemplateService templateService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getHome() {		
		return "home";
	}
	
	@RequestMapping(value = "/templates", method = RequestMethod.POST)
	public @ResponseBody List<Template> templates(@RequestBody TemplateDTO templateDTO,
			HttpServletRequest request, HttpServletResponse response) {
		logger.info(templateDTO.toString());
		//displayHeaderNames(request);
		
		List<Template> matchingTemplates = templateService.getMatchingTemplates(templateDTO, request, response);		
		return matchingTemplates;
	}

	private void displayHeaderNames(HttpServletRequest request) {
		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			Object header = (Object) headerNames.nextElement();
			logger.info("header: " + header);
		}
	}	
}
