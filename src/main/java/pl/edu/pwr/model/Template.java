package pl.edu.pwr.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="template")
@NamedQuery(name="Template.findAll", query="SELECT t FROM Template t")
public class Template implements Serializable {

	private static final long serialVersionUID = 4554171148574141659L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="accept_encoding_header")
	private String acceptEncodingHeader;

	@Column(name="accept_header")
	private String acceptHeader;

	@Column(name="accept_language_header")
	private String acceptLanguageHeader;

	@Column(name="browser_name")
	private String browserName;

	@Column(name="cookie_enabled")
	private Boolean cookieEnabled;

	@Column(name="do_not_track")
	private String doNotTrack;

	@Column(name="finger_print_id")
	private String fingerPrintId;

	@Column(name="hash_canvas")
	private String hashCanvas;

	@Column(name="ip_address")
	private String ipAddress;

	@Column(name="java_enabled")
	private Boolean javaEnabled;

	private String language;

	private String platform;

	@Lob
	private String plugins;

	private String resolution;

	@Column(name="time_zone_offset")
	private String timeZoneOffset;

	@Column(name="user_agent")
	private String userAgent;
	
	@Transient
	private String matchingValue;

	public Template() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAcceptEncodingHeader() {
		return this.acceptEncodingHeader;
	}

	public void setAcceptEncodingHeader(String acceptEncodingHeader) {
		this.acceptEncodingHeader = acceptEncodingHeader;
	}

	public String getAcceptHeader() {
		return this.acceptHeader;
	}

	public void setAcceptHeader(String acceptHeader) {
		this.acceptHeader = acceptHeader;
	}

	public String getAcceptLanguageHeader() {
		return this.acceptLanguageHeader;
	}

	public void setAcceptLanguageHeader(String acceptLanguageHeader) {
		this.acceptLanguageHeader = acceptLanguageHeader;
	}

	public String getBrowserName() {
		return this.browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public Boolean getCookieEnabled() {
		return this.cookieEnabled;
	}

	public void setCookieEnabled(Boolean cookieEnabled) {
		this.cookieEnabled = cookieEnabled;
	}

	public String getDoNotTrack() {
		return this.doNotTrack;
	}

	public void setDoNotTrack(String doNotTrack) {
		this.doNotTrack = doNotTrack;
	}

	public String getFingerPrintId() {
		return this.fingerPrintId;
	}

	public void setFingerPrintId(String fingerPrintId) {
		this.fingerPrintId = fingerPrintId;
	}

	public String getHashCanvas() {
		return this.hashCanvas;
	}

	public void setHashCanvas(String hashCanvas) {
		this.hashCanvas = hashCanvas;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Boolean getJavaEnabled() {
		return this.javaEnabled;
	}

	public void setJavaEnabled(Boolean javaEnabled) {
		this.javaEnabled = javaEnabled;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPlatform() {
		return this.platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPlugins() {
		return this.plugins;
	}

	public void setPlugins(String plugins) {
		this.plugins = plugins;
	}

	public String getResolution() {
		return this.resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getTimeZoneOffset() {
		return this.timeZoneOffset;
	}

	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getMatchingValue() {
		return matchingValue;
	}

	public void setMatchingValue(String matchingValue) {
		this.matchingValue = matchingValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acceptEncodingHeader == null) ? 0 : acceptEncodingHeader.hashCode());
		result = prime * result + ((acceptHeader == null) ? 0 : acceptHeader.hashCode());
		result = prime * result + ((acceptLanguageHeader == null) ? 0 : acceptLanguageHeader.hashCode());
		result = prime * result + ((browserName == null) ? 0 : browserName.hashCode());
		result = prime * result + ((cookieEnabled == null) ? 0 : cookieEnabled.hashCode());
		result = prime * result + ((doNotTrack == null) ? 0 : doNotTrack.hashCode());
		result = prime * result + ((fingerPrintId == null) ? 0 : fingerPrintId.hashCode());
		result = prime * result + ((hashCanvas == null) ? 0 : hashCanvas.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((javaEnabled == null) ? 0 : javaEnabled.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((platform == null) ? 0 : platform.hashCode());
		result = prime * result + ((plugins == null) ? 0 : plugins.hashCode());
		result = prime * result + ((resolution == null) ? 0 : resolution.hashCode());
		result = prime * result + ((timeZoneOffset == null) ? 0 : timeZoneOffset.hashCode());
		result = prime * result + ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Template))
			return false;
		Template other = (Template) obj;
		if (acceptEncodingHeader == null) {
			if (other.acceptEncodingHeader != null)
				return false;
		} else if (!acceptEncodingHeader.equals(other.acceptEncodingHeader))
			return false;
		if (acceptHeader == null) {
			if (other.acceptHeader != null)
				return false;
		} else if (!acceptHeader.equals(other.acceptHeader))
			return false;
		if (acceptLanguageHeader == null) {
			if (other.acceptLanguageHeader != null)
				return false;
		} else if (!acceptLanguageHeader.equals(other.acceptLanguageHeader))
			return false;
		if (browserName == null) {
			if (other.browserName != null)
				return false;
		} else if (!browserName.equals(other.browserName))
			return false;
		if (cookieEnabled == null) {
			if (other.cookieEnabled != null)
				return false;
		} else if (!cookieEnabled.equals(other.cookieEnabled))
			return false;
		if (doNotTrack == null) {
			if (other.doNotTrack != null)
				return false;
		} else if (!doNotTrack.equals(other.doNotTrack))
			return false;
		if (fingerPrintId == null) {
			if (other.fingerPrintId != null)
				return false;
		} else if (!fingerPrintId.equals(other.fingerPrintId))
			return false;
		if (hashCanvas == null) {
			if (other.hashCanvas != null)
				return false;
		} else if (!hashCanvas.equals(other.hashCanvas))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (javaEnabled == null) {
			if (other.javaEnabled != null)
				return false;
		} else if (!javaEnabled.equals(other.javaEnabled))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (platform == null) {
			if (other.platform != null)
				return false;
		} else if (!platform.equals(other.platform))
			return false;
		if (plugins == null) {
			if (other.plugins != null)
				return false;
		} else if (!plugins.equals(other.plugins))
			return false;
		if (resolution == null) {
			if (other.resolution != null)
				return false;
		} else if (!resolution.equals(other.resolution))
			return false;
		if (timeZoneOffset == null) {
			if (other.timeZoneOffset != null)
				return false;
		} else if (!timeZoneOffset.equals(other.timeZoneOffset))
			return false;
		if (userAgent == null) {
			if (other.userAgent != null)
				return false;
		} else if (!userAgent.equals(other.userAgent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Template [id=" + id + ", acceptEncodingHeader=" + acceptEncodingHeader + ", acceptHeader="
				+ acceptHeader + ", acceptLanguageHeader=" + acceptLanguageHeader + ", browserName=" + browserName
				+ ", cookieEnabled=" + cookieEnabled + ", doNotTrack=" + doNotTrack + ", fingerPrintId=" + fingerPrintId
				+ ", hashCanvas=" + hashCanvas + ", ipAddress=" + ipAddress + ", javaEnabled=" + javaEnabled
				+ ", language=" + language + ", platform=" + platform + ", plugins=" + plugins + ", resolution="
				+ resolution + ", timeZoneOffset=" + timeZoneOffset + ", userAgent=" + userAgent + ", matchingValue="
				+ matchingValue + "]";
	}
}