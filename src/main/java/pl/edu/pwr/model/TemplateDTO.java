package pl.edu.pwr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TemplateDTO implements Serializable{

	private static final long serialVersionUID = 1743103952942401447L;
	
	private String ipAddress;
	private String resolution;
	private String timeZoneOffset;
	private String userAgent;
	private String language;
	private Boolean cookieEnabled;
	private Boolean javaEnabled;
	private String doNotTrack;
	private String platform;	
	private String hashCanvas;
	private String browserName;
	private List<Plugin> plugins = new ArrayList<Plugin>();// z tego trzeba zrobic jakiegos Stringa, zeby to wrzucic do bazy...
	
	public List<Plugin> getPlugins() {
		return plugins;
	}
	public void setPlugins(List<Plugin> plugins) {
		this.plugins = plugins;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}	
	public String getBrowserName() {
		return browserName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	public String getHashCanvas() {
		return hashCanvas;
	}
	public void setHashCanvas(String hashCanvas) {
		this.hashCanvas = hashCanvas;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}
	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Boolean getCookieEnabled() {
		return cookieEnabled;
	}
	public void setCookieEnabled(Boolean cookieEnabled) {
		this.cookieEnabled = cookieEnabled;
	}
	public Boolean getJavaEnabled() {
		return javaEnabled;
	}
	public void setJavaEnabled(Boolean javaEnabled) {
		this.javaEnabled = javaEnabled;
	}
	public String getDoNotTrack() {
		return doNotTrack;
	}
	public void setDoNotTrack(String doNotTrack) {
		this.doNotTrack = doNotTrack;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((browserName == null) ? 0 : browserName.hashCode());
		result = prime * result + ((cookieEnabled == null) ? 0 : cookieEnabled.hashCode());
		result = prime * result + ((doNotTrack == null) ? 0 : doNotTrack.hashCode());
		result = prime * result + ((hashCanvas == null) ? 0 : hashCanvas.hashCode());
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((javaEnabled == null) ? 0 : javaEnabled.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((platform == null) ? 0 : platform.hashCode());
		result = prime * result + ((resolution == null) ? 0 : resolution.hashCode());
		result = prime * result + ((timeZoneOffset == null) ? 0 : timeZoneOffset.hashCode());
		result = prime * result + ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TemplateDTO))
			return false;
		TemplateDTO other = (TemplateDTO) obj;
		if (browserName == null) {
			if (other.browserName != null)
				return false;
		} else if (!browserName.equals(other.browserName))
			return false;
		if (cookieEnabled == null) {
			if (other.cookieEnabled != null)
				return false;
		} else if (!cookieEnabled.equals(other.cookieEnabled))
			return false;
		if (doNotTrack == null) {
			if (other.doNotTrack != null)
				return false;
		} else if (!doNotTrack.equals(other.doNotTrack))
			return false;
		if (hashCanvas == null) {
			if (other.hashCanvas != null)
				return false;
		} else if (!hashCanvas.equals(other.hashCanvas))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (javaEnabled == null) {
			if (other.javaEnabled != null)
				return false;
		} else if (!javaEnabled.equals(other.javaEnabled))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (platform == null) {
			if (other.platform != null)
				return false;
		} else if (!platform.equals(other.platform))
			return false;
		if (resolution == null) {
			if (other.resolution != null)
				return false;
		} else if (!resolution.equals(other.resolution))
			return false;
		if (timeZoneOffset == null) {
			if (other.timeZoneOffset != null)
				return false;
		} else if (!timeZoneOffset.equals(other.timeZoneOffset))
			return false;
		if (userAgent == null) {
			if (other.userAgent != null)
				return false;
		} else if (!userAgent.equals(other.userAgent))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TemplateDTO [ipAddress=" + ipAddress + ", resolution=" + resolution + ", timeZoneOffset="
				+ timeZoneOffset + ", userAgent=" + userAgent + ", language=" + language + ", cookieEnabled="
				+ cookieEnabled + ", javaEnabled=" + javaEnabled + ", doNotTrack=" + doNotTrack + ", platform="
				+ platform + ", hashCanvas=" + hashCanvas + ", browserName=" + browserName + ", plugins=" + plugins
				+ "]";
	}	
}
