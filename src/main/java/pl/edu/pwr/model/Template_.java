package pl.edu.pwr.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-11-25T01:43:35.874+0100")
@StaticMetamodel(Template.class)
public class Template_ {
	public static volatile SingularAttribute<Template, Integer> id;
	public static volatile SingularAttribute<Template, String> acceptEncodingHeader;
	public static volatile SingularAttribute<Template, String> acceptHeader;
	public static volatile SingularAttribute<Template, String> acceptLanguageHeader;
	public static volatile SingularAttribute<Template, String> browserName;
	public static volatile SingularAttribute<Template, Boolean> cookieEnabled;
	public static volatile SingularAttribute<Template, String> doNotTrack;
	public static volatile SingularAttribute<Template, String> fingerPrintId;
	public static volatile SingularAttribute<Template, String> hashCanvas;
	public static volatile SingularAttribute<Template, String> ipAddress;
	public static volatile SingularAttribute<Template, Boolean> javaEnabled;
	public static volatile SingularAttribute<Template, String> language;
	public static volatile SingularAttribute<Template, String> platform;
	public static volatile SingularAttribute<Template, String> plugins;
	public static volatile SingularAttribute<Template, String> resolution;
	public static volatile SingularAttribute<Template, String> timeZoneOffset;
	public static volatile SingularAttribute<Template, String> userAgent;
}
