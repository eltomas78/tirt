package pl.edu.pwr.repository;

import java.util.List;

import pl.edu.pwr.model.Template;

public interface TemplateRepository {
	
	List<Template> getAllTemplates();
	List<Template> getAllTemplatesByAttribute(String attributeName, Object attributeValue);
	
	Template getTemplateById(Integer id);
	
	Boolean isTemplateAdded(Template template);
	Boolean isTemplateUpdated(Integer templateId, Template template);
	Boolean isTemplateDeleted(Integer templateId);

}
