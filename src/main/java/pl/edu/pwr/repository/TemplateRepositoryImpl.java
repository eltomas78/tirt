package pl.edu.pwr.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import pl.edu.pwr.model.Template;

@Repository
@Transactional
public class TemplateRepositoryImpl implements TemplateRepository {
	
	@PersistenceContext
	private EntityManager em;

	//@SuppressWarnings("unchecked")
	@Override
	public List<Template> getAllTemplates() {
		List<Template> resultList = em.createNamedQuery("Template.findAll", Template.class).getResultList();//!!! Sprawdź czy to zadziała !!!
		return resultList;
	}

	@Override
	public Template getTemplateById(Integer id) {
		return em.find(Template.class, id);
	}

	@Override
	public Boolean isTemplateAdded(Template template) {
		if (template != null) {
			em.persist(template);
			return true;
		}
		return false;
	}

	@Override
	public Boolean isTemplateUpdated(Integer templateId, Template template) {
		Template existingTemplate = getTemplateById(templateId);
		if (template != null && existingTemplate != null) {
			existingTemplate = setValuesInTemplates(existingTemplate, template);
			em.merge(existingTemplate);
			return true;
		}
		return false;		
	}

	private Template setValuesInTemplates(Template oldTemplate, Template newTemplate) {
		oldTemplate.setAcceptEncodingHeader(newTemplate.getAcceptEncodingHeader());
		oldTemplate.setAcceptHeader(newTemplate.getAcceptHeader());
		oldTemplate.setAcceptLanguageHeader(newTemplate.getAcceptLanguageHeader());
		oldTemplate.setBrowserName(newTemplate.getBrowserName());
		oldTemplate.setCookieEnabled(newTemplate.getCookieEnabled());
		oldTemplate.setDoNotTrack(newTemplate.getDoNotTrack());
		oldTemplate.setFingerPrintId(newTemplate.getFingerPrintId());
		oldTemplate.setHashCanvas(newTemplate.getHashCanvas());
		oldTemplate.setIpAddress(newTemplate.getIpAddress());
		oldTemplate.setJavaEnabled(newTemplate.getJavaEnabled());
		oldTemplate.setLanguage(newTemplate.getLanguage());
		oldTemplate.setLanguage(newTemplate.getLanguage());
		oldTemplate.setPlatform(newTemplate.getPlatform());
		oldTemplate.setPlugins(newTemplate.getPlugins());
		oldTemplate.setResolution(newTemplate.getResolution());
		oldTemplate.setTimeZoneOffset(newTemplate.getTimeZoneOffset());
		oldTemplate.setUserAgent(newTemplate.getUserAgent());
		return oldTemplate;
	}

	@Override
	public Boolean isTemplateDeleted(Integer templateId) {
		Template template = getTemplateById(templateId);
		if (template != null) {
			em.remove(template);
			return true;
		}
		return false;
	}

	@Override
	public List<Template> getAllTemplatesByAttribute(String attributeName, Object attributeValue) {
		List<Template> resultList = em
				.createQuery("SELECT t FROM Template t where t." + attributeName + " =:" + attributeName, Template.class)
				.setParameter(attributeName, attributeValue)
				.getResultList();
		return resultList;
	}
}
