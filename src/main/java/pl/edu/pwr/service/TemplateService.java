package pl.edu.pwr.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.edu.pwr.model.Template;
import pl.edu.pwr.model.TemplateDTO;

public interface TemplateService {

	List<Template> getMatchingTemplates(TemplateDTO templateDTO, HttpServletRequest request, HttpServletResponse response);
	

}
