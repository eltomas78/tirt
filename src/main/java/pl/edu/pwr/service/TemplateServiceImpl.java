package pl.edu.pwr.service;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.pwr.model.Plugin;
import pl.edu.pwr.model.Template;
import pl.edu.pwr.model.TemplateDTO;
import pl.edu.pwr.repository.TemplateRepository;

@Service
@Transactional
public class TemplateServiceImpl implements TemplateService {
	
	private static final Logger logger = LoggerFactory.getLogger(TemplateServiceImpl.class);
	private SecureRandom random = new SecureRandom();
	
	private final String COOKIE_NAME_FINGER_PRINT_ID = "fingerPrintId";
	private final String HASH_CANVAS = "hashCanvas";
	private final Integer ONE_YEAR = 31556926;
	private final Double ATTRIBUTES_QUANTITY = 16.0;
	private final Double ACCEPTABLE_RATIO = 65.0;
	
	@Autowired
	private TemplateRepository templateRepository;
	
	@Override
	public List<Template> getMatchingTemplates(TemplateDTO templateDTO, HttpServletRequest request, HttpServletResponse response) {
		
		List<Template> templates = new ArrayList<Template>();
		Cookie cookieWithFingerPrintId = setCookieFingerPrintIdToResponseIfMissing(request, response);		
		Template template = setValuesFromDTOAndRequest(templateDTO, request, cookieWithFingerPrintId);
		templates.add(template);
		
		List<Template> templateWithKnownFingerPrint = templateRepository.getAllTemplatesByAttribute(COOKIE_NAME_FINGER_PRINT_ID, template.getFingerPrintId());
		List<Template> templateWithKnownHashCanvas = templateRepository.getAllTemplatesByAttribute(HASH_CANVAS, template.getHashCanvas());		
		if (templateWithKnownFingerPrint.size() == 1) {
			Template templateTemp = templateWithKnownFingerPrint.get(0);
			if (!templates.contains(templateTemp)) {				
				templateTemp.setMatchingValue("100.00%");
				templates.add(templateTemp);	
			}			
			//return templates;
		}		
		if (templateWithKnownHashCanvas.size() == 1) {
			Template templateTemp = templateWithKnownHashCanvas.get(0);
			if (!templates.contains(templateTemp)) {				
				templateTemp.setMatchingValue("100.00%");
				templates.add(templateTemp);	
			}	
			//return templates;
		}
		
		Map<Integer, Integer> querredTemplates = null;
		try {
			querredTemplates = getQuerredTemplates(template);
		} catch (IllegalArgumentException e) {			
			e.printStackTrace();
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		}	
		
		
		for (Map.Entry<Integer, Integer> entry : querredTemplates.entrySet()){
		    double matchingAttributesRatio = (entry.getValue() / ATTRIBUTES_QUANTITY) * 100;
		    //System.out.println(entry.getKey() + "/" + entry.getValue() + ", ratio: " + matchingAttributesRatio);   		    
		    	
	    	Template templateById = templateRepository.getTemplateById(entry.getKey());			
			if (!templates.contains(templateById)) {				
				String matchingValue = Double.toString(matchingAttributesRatio) + "%";
				templateById.setMatchingValue(matchingValue);
				templates.add(templateById);
			}		    
		}
		
		int maxValue = -1;		
		Collection<Integer> values = querredTemplates.values();
		if (!values.isEmpty()) {
			maxValue = Collections.max(values);
		}	
		double matchingAttributesRatio = (maxValue / ATTRIBUTES_QUANTITY) * 100;
		//System.out.println("maxValue = " + maxValue + ", matchingAttributesRatio = " + matchingAttributesRatio);
		if (querredTemplates.isEmpty() || (matchingAttributesRatio > 0.0 && matchingAttributesRatio <= ACCEPTABLE_RATIO)) {//jak nie ma zadnego wpisu w bazie to musze dodac pierwszy rekord jakos, zeby pozniej mogl byc porownywany z innymi
			templateRepository.isTemplateAdded(template);			
		}		
		
		return templates;		
	}
	
	private Cookie setCookieFingerPrintIdToResponseIfMissing(HttpServletRequest request, HttpServletResponse response) {		
		Cookie cookie = new Cookie(COOKIE_NAME_FINGER_PRINT_ID, nextSessionId().toUpperCase());
		cookie.setMaxAge(ONE_YEAR);	
		Cookie cookieWithFingerPrintId = cookie;
		Cookie[] cookies = request.getCookies();
		boolean cookieFound = false;
		if (cookies != null) {			
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(COOKIE_NAME_FINGER_PRINT_ID)) {
					cookieWithFingerPrintId = cookies[i];
					cookieFound = true;
					break;
				}
			}
		}
		if (!cookieFound) {
			response.addCookie(cookie);
		}	
		return cookieWithFingerPrintId;
	}
	
	private String nextSessionId() {
		return new BigInteger(130, random).toString(32);
	}		
	
	private Template setValuesFromDTOAndRequest(TemplateDTO templateDTO, HttpServletRequest request, Cookie cookieWithFingerPrintId) {
		Template template = new Template();		
		String acceptHeader = request.getHeader("accept");
		String acceptEncodingHeader = request.getHeader("accept-encoding");
		String acceptLanguageHeader = request.getHeader("accept-language");
		//String cookieHeader = request.getHeader("cookie");
		
		template.setFingerPrintId(cookieWithFingerPrintId.getValue());
		
		template.setAcceptEncodingHeader(acceptEncodingHeader);
		template.setAcceptHeader(acceptHeader);
		template.setAcceptLanguageHeader(acceptLanguageHeader);
		
		template.setBrowserName(templateDTO.getBrowserName());
		template.setCookieEnabled(templateDTO.getCookieEnabled());
		template.setDoNotTrack(templateDTO.getDoNotTrack());		
		template.setHashCanvas(templateDTO.getHashCanvas());
		template.setIpAddress(templateDTO.getIpAddress());
		template.setJavaEnabled(templateDTO.getJavaEnabled());
		template.setLanguage(templateDTO.getLanguage());
		template.setLanguage(templateDTO.getLanguage());
		template.setPlatform(templateDTO.getPlatform());		
		template.setResolution(templateDTO.getResolution());
		template.setTimeZoneOffset(templateDTO.getTimeZoneOffset());
		template.setUserAgent(templateDTO.getUserAgent());
		template.setPlugins(setPluginsToString(templateDTO.getPlugins()));		
		
		return template;
	}
	
	private Map<Integer, Integer> getQuerredTemplates(Template template) throws IllegalArgumentException, IllegalAccessException{		
		Boolean ACCESS_TO_PRIVATE_FIELDS = true;
		Map<Integer, Integer> querredTemplates = new HashMap<Integer, Integer>();
		Field[] declaredFields = Template.class.getDeclaredFields();
		for (int i = 0; i < declaredFields.length; i++) {
			Field field = declaredFields[i];
			field.setAccessible(ACCESS_TO_PRIVATE_FIELDS);			
			int fieldModifier = field.getModifiers();
			if (fieldModifier == Modifier.PRIVATE && !field.getName().matches("(id|matchingValue)")) {					
				List<Template> templatesByAttribute = templateRepository.getAllTemplatesByAttribute(field.getName(), field.get(template));				
				for (Template newTemplate : templatesByAttribute) {
					int newTemplateId = newTemplate.getId();
					if (querredTemplates.containsKey(newTemplateId)) {
						int newTemplateValue = querredTemplates.get(newTemplateId);						
						newTemplateValue++;						
						querredTemplates.put(newTemplateId, newTemplateValue);
					}
					else{
						querredTemplates.put(newTemplateId, 1);
					}
				}
			}			
		}		
		return querredTemplates;
	}
	
	private String setPluginsToString(List<Plugin> plugins) {
		StringBuilder sb = new StringBuilder();
		for (Plugin plugin : plugins) {
			String pluginName = plugin.getName();
			if (pluginName != null && !pluginName.matches("(item|namedItem|refresh)") && sb.indexOf(pluginName) == -1) {
				sb.append("-" + pluginName + "\n");		
			}			
		}
		return sb.toString();
	}
}