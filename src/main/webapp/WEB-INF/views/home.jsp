<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="pl" ng-app="tirtApp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" /> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<spring:url value="/resources/lib/jquery/jquery-3.1.1.min.js" var="jqueryJs" />
<spring:url value="/resources/lib/angular/angular.min.js" var="angularJs" />
<spring:url value="/resources/lib/angular/angular-route.min.js" var="angularRouteJs" />
<spring:url value="/resources/lib/bootstrap/js/bootstrap.min.js" var="bootstrapJs" />
<spring:url value="/resources/lib/fingerprint.js" var="fingerPrintJs" />
<spring:url value="/resources/app/modules/mainModule.js" var="ngMainModule" />
<spring:url value="/resources/app/modules/servicesModule.js"	var="ngServicesModule" />
<spring:url value="/resources/app/modules/controllersModule.js" var="ngControllersModule" />
<spring:url value="/resources/app/services/mainService.js" var="ngMainService" />
<spring:url value="/resources/app/controllers/mainController.js" var="ngMainController" />
<spring:url value="/resources/app/utils.js" var="utilsJs" />


<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css" var="bootstrapCss" />
<spring:url	value="/resources/lib/bootstrap/css/bootstrap-theme.min.css"	var="bootstrapThemeCss" />

<!-- libraries -->
<script src="${jqueryJs}"></script>
<script src="${angularJs}"></script>
<script src="${angularRouteJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${fingerPrintJs}"></script>

<!-- ng-modules -->
<script src="${ngMainModule}"></script>
<script src="${ngServicesModule}"></script>
<script src="${ngControllersModule}"></script>

<!-- ng-services -->
<script src="${ngMainService}"></script>

<!-- ng-controllers -->
<script src="${ngMainController}"></script>

<!-- other -->
<script src="${utilsJs}"></script>

<link rel="stylesheet" href="${bootstrapCss}">
<link rel="stylesheet" href="${bootstrapThemeCss}">

<title>TIRT - Browser Fingerprint</title>

</head>
<body ng-controller="mainController">
	<div class="container-fluid">
		<!-- <button onclick="sendData()">Przycisk</button>	 -->
		<%-- <canvas id="myCanvas" width="200" height="100"></canvas> --%>
		<div id="canvas"></div>			
		<ng-view />	
	</div>
</body>
</html>
