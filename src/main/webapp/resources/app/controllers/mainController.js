controllersModule.controller('mainController', function($scope, mainService){	
	
	$scope.template = {};
	$scope.returnedTemplate = {};
	$scope.returnedTemplates = [];
	$scope.dataLoaded = false;
	
	var obtainIpAddress = function(){
		mainService.getIpAddress()
			.then(function(data){
				$scope.template.ipAddress = data.data.ip;
				$scope.template.resolution = obtainScreenResolution();
				$scope.template.timeZoneOffset = getTimeZoneOffset();
				$scope.template.userAgent = getUserAgent();
				$scope.template.language = getLanguage();
				$scope.template.cookieEnabled = isCookieEnabled();
				$scope.template.javaEnabled = isJavaEnabled();
				$scope.template.doNotTrack = getDoNotTrack();
				$scope.template.plugins = getPlugins();
				$scope.template.hashCanvas = getCanvasHash();
				$scope.template.platform = getPlatform();
				$scope.template.browserName = getBrowserName();
				//$scope.template.fp1 = new Fingerprint().get();
				//$scope.template.fp2 = new Fingerprint({canvas: true}).get();
				//$scope.template.fp3 = new Fingerprint({ie_activex: true}).get();
				//$scope.template.fp4 = new Fingerprint({screen_resolution: true}).get();
			
				//console.log('$scope.template: ' + JSON.stringify($scope.template));				
				mainService.sendTemplate($scope.template)
					.then(function(data){
						$scope.dataLoaded = true;
						$scope.returnedTemplates = data.data;
						$scope.returnedTemplate = $scope.returnedTemplates[0];
						$scope.returnedTemplates = $scope.returnedTemplates.slice(1);
						//console.log("$scope.returnedTemplate = " + JSON.stringify($scope.returnedTemplate) + "\n\n$scope.returnedTemplates = " + JSON.stringify($scope.returnedTemplates));
			});			
		});
	}
	
	var obtainScreenResolution = function(){		
		var screenWidth = window.screen.availWidth;
		var screenHeight = window.screen.availHeight;
		var screenColorDepth = window.screen.colorDepth;
		var screenResolution = screenWidth + "x" + screenHeight + "x" + screenColorDepth;
		return screenResolution;
	}
	
	var getTimeZoneOffset = function(){
		return new Date().getTimezoneOffset();
	}
	
	var getBrowserName = function(){
		var browserName = "unknown";
		if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent
				.indexOf('OPR')) != -1) {
			browserName = "Opera";
		} else if (navigator.userAgent.indexOf("Chrome") != -1) {
			browserName = "Chrome";
		} else if (navigator.userAgent.indexOf("Safari") != -1) {
			browserName = "Safari";
		} else if (navigator.userAgent.indexOf("Firefox") != -1) {
			browserName = "Firefox";
		} else if ((navigator.userAgent.indexOf("MSIE") != -1)
				|| (!!document.documentMode == true)) //IF IE > 10
		{
			browserName = "IE";
		}
		return browserName;
	}
	
	var getUserAgent = function(){
		return window.navigator.userAgent;
	}
	
	var getLanguage = function(){
		return window.navigator.language;
	}
	
	var getPlatform = function(){		
		return window.navigator.platform;
	}
	
	var isCookieEnabled = function(){
		return window.navigator.cookieEnabled;
	}	
	
	var isJavaEnabled = function(){
		return window.navigator.javaEnabled();
	}	
	
	var getDoNotTrack = function(){
		return window.navigator.doNotTrack;
	}
	
	var getPlugins = function(){
		var myPlugins = [];
		for(var property in window.navigator.plugins){			
		    var plugin = window.navigator.plugins[property];
		    var myPlugin = {};
		    myPlugin.name = plugin.name;
		    myPlugin.fileName = plugin.filename;
		    myPlugin.description = plugin.description;		    
		    myPlugins.push(myPlugin);
		} 
		return myPlugins;
	}
	
	var getCanvasHash = function() {
	    var canvas = document.createElement('canvas');
	    var ctx = canvas.getContext('2d');
	    var txt = 'i9asdm..$#po((^@KbXrww!~cz';
	    ctx.textBaseline = "top";
	    ctx.font = "16px 'Arial'";
	    ctx.textBaseline = "alphabetic";
	    ctx.rotate(.05);
	    ctx.fillStyle = "#f60";
	    ctx.fillRect(125,1,62,20);
	    ctx.fillStyle = "#069";
	    ctx.fillText(txt, 2, 15);
	    ctx.fillStyle = "rgba(102, 200, 0, 0.7)";
	    ctx.fillText(txt, 4, 17);
	    ctx.shadowBlur=10;
	    ctx.shadowColor="blue";
	    ctx.fillRect(-20,10,234,5);
	    var strng=canvas.toDataURL();

	    var hash=0;
	    if (strng.length==0) return;
	    for (i = 0; i < strng.length; i++) {
	        char = strng.charCodeAt(i);
	        hash = ((hash<<5)-hash)+char;
	        hash = hash & hash;
	    }
	    return hash;
	}
	
	$scope.returnFontSyle = function(returnedTemplateValue, templateValue){		
		var index = typeof templateValue == "string" && templateValue.indexOf("%");		
		var templateStyle = {color:'black'};		
		var isDanger = returnedTemplateValue !== templateValue;			
		var isSuccess = returnedTemplateValue === templateValue;	
		//console.log("returnedTemplateValue: " + returnedTemplateValue + ", templateValue: " + templateValue + ", typ: " + typ + ", index: " + index);		
		

		if(isDanger){
			templateStyle.color = "red";
		} else if(isSuccess){
			templateStyle.color = "green";
		}

		if (index && index !== -1){			
			var valueStr = templateValue.substring(0, index);			
			var value = Number(valueStr);
			
			if (value < 60.00) {
				templateStyle.color = "red";
			} else if (value >= 60.00 && value < 90.00){
				templateStyle.color = "orange";
			} else if (value >= 90.00 && value < 100.00) {
				templateStyle.color = "blue";
			} else {
				templateStyle.color = "green";
			}
		}
		
		return templateStyle;
	}
	
	obtainIpAddress();	
});