var tirtApp = angular.module('tirtApp', ['ngRoute', 'servicesModule', 'controllersModule']);

tirtApp.constant("baseUrl", "/pwr/resources/app/views/");	

tirtApp.config(function($routeProvider, baseUrl) {
	
	$routeProvider.when("/main", {
		templateUrl: baseUrl + "main.html",
		//controller: 'mainController'				
	});	
	$routeProvider.otherwise({
		redirectTo: "/main"
	});
});