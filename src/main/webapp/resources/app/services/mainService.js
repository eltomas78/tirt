servicesModule.factory('mainService', function($http){	
	
	var displayMessage = function(message){
		$log.log(message);
	};
	
	var getIpAddress = function(){
		return $http.get("http://jsonip.com/?callback=");
	}
	
	var sendTemplate = function(templateDto){
		return $http.post("http://localhost:8080/pwr/templates", templateDto);
	}

	return {
		log : displayMessage,
		getIpAddress : getIpAddress,
		sendTemplate: sendTemplate
	};
});