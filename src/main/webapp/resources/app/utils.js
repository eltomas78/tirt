var sendData = function(){
	console.log('button clicked!');
	
	
	
	var screenHeight = window.screen.availHeight;
	var screenWidth = window.screen.availWidth;
	var screenColorDepth = window.screen.colorDepth;
	console.log('screenWidth = ' + screenWidth + ', screenHeight = ' + screenHeight + ', screenColorDepth = ' + screenColorDepth);
	var offset = new Date().getTimezoneOffset();
	console.log('offset = ' + offset);
	var uagent = navigator.userAgent;
	console.log('uagent = ' + uagent);
	var language = window.navigator.language;
	console.log('language = ' + language);
	var cookieEnabled = window.navigator.cookieEnabled;
	console.log('cookieEnabled = ' + cookieEnabled);
	var javaEnabled = window.navigator.javaEnabled();
	console.log('javaEnabled = ' + javaEnabled);
	var historyLength = history.length;
	console.log('historyLength = ' + historyLength);
	var doNotTrack = window.navigator.doNotTrack;
	console.log('doNotTrack = ' + doNotTrack);
	var windowNavigator = window.navigator;
	console.log('windowNavigator = ' + JSON.stringify(windowNavigator));
	document.write("<br />NAVIGATOR:<br />")
	for(var property in windowNavigator){ 
	    var str = windowNavigator[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");
	} 
	document.write("<br />WINDOW:<br />")
	var windowWindow = window;
	console.log('windowWindow = ' + windowWindow);
	for(var property in windowWindow){ 
	    var str = windowWindow[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");
	} 
	document.write("<br />WINDOW.SESSION_STORAGE:<br />")
	for(var property in windowWindow.sessionStorage){ 
	    var str = windowWindow.sessionStorage[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");
	} 
	document.write("<br />WINDOW.LOCAL_STORAGE:<br />")
	for(var property in windowWindow.localStorage){ 
	    var str = windowWindow.localStorage[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");
	} 
	document.write("<br />WINDOW SCREEN:<br />")
	var windowScreen = window.screen;
	console.log('windowScreen = ' + JSON.stringify(windowScreen));
	for(var property in windowScreen){ 
	    var str = windowScreen[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");
	} 
	document.write("<br />PLUGINS:<br />")
	for(var property in windowNavigator.plugins){ 
	    var str = windowNavigator.plugins[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str.name+"</em><br />");
	    document.write(property+ "&nbsp;&nbsp;<em>"+str.filename+"</em><br />");
	    document.write(property+ "&nbsp;&nbsp;<em>"+str.description+"</em><br />");	    
	} 
	document.write("<br />MIME_TYPES:<br />")
	for(var property in windowNavigator.mimeTypes){ 
	    var str = windowNavigator.mimeTypes[property]
	    document.write(property+ "&nbsp;&nbsp;<em>"+str+"</em><br />");	    
	} 
}