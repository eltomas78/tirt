-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 25 Lis 2016, 01:29
-- Wersja serwera: 5.5.53-0+deb8u1
-- Wersja PHP: 5.6.27-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `tirt`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `template`
--

CREATE TABLE IF NOT EXISTS `template` (
`id` int(11) NOT NULL,
  `ip_address` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `resolution` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `time_zone_offset` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `cookie_enabled` tinyint(1) NOT NULL,
  `java_enabled` tinyint(1) NOT NULL,
  `do_not_track` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `platform` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `hash_canvas` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `browser_name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `plugins` text COLLATE utf8_polish_ci NOT NULL,
  `accept_header` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `accept_encoding_header` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `accept_language_header` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `finger_print_id` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `template`
--

INSERT INTO `template` (`id`, `ip_address`, `resolution`, `time_zone_offset`, `user_agent`, `language`, `cookie_enabled`, `java_enabled`, `do_not_track`, `platform`, `hash_canvas`, `browser_name`, `plugins`, `accept_header`, `accept_encoding_header`, `accept_language_header`, `finger_print_id`) VALUES
(1, 'sdf', 'dfd', 'dfd', 'asdfd', 'sdf', 0, 1, 'fghgf', 'dfghgf', 'fghfh', 'fhfgh', 'fghfdgh dgfh gdgh dgf', 'fdhgfdh', 'fghfgdh', 'fdghgf', 'dgfhgh');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `template`
--
ALTER TABLE `template`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `template`
--
ALTER TABLE `template`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
